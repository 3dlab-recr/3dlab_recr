#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <QDebug>
#include <QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->Button, &QPushButton::pressed, this, &MainWindow::run_script);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::run_script()
{
    QString answer;
    QProcess rs;
    QString loc = QDir::homePath();
    QString script = loc+"/3DLAB_recr/Resources/get_ip.sh";
    rs.start("/bin/sh", QStringList() << script);
    if (rs.waitForFinished())
    {
        answer = rs.readLine().trimmed();;
        ui->message_label->setText(answer);
    }
}
